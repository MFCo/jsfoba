JSFObA es una aplicación desarrollada en JavaScript y Java que permite al usuario conseguir una representación en forma de grafo de las relaciones existentes entre los objetos de un sistema JavaScript dado.

Dicho sistema debe poder ser ejecutado en Node.js, y además ser un sistema monolítico.

Para realizar dicho análisis se utiliza el módulo Aran, el cual permite realizar el instrumentado de código JavaScript a través de un sistema de trampas, permitiendo de esta forma ejecutar acciones personalizadas ante ciertos eventos.

Esto da lugar al puntapie inicial, el generar un árbol de invocaciones de aplicaciones JavaScript, el cuál sea generado de forma dinámica en tiempo de ejecución de la aplicación, permitiendo así detectar los llamados que realmente se llevan a cabo.

En una primer etapa la aplicación cuenta con una parte en JavaScript, la cual corre sobre la plataforma Node.js, en la cual se genera el código instrumentado. Luego el mismo es ejecutado, generando el árbol de invocaciones en tiempo de ejecución deseado.

Luego de esto, el árbol generado es interpretado y visualizado por una segunda etapa desarrollada en Java, la cual finaliza proyectando un grafo que muestra la relación entre cada miembro de cada objeto.

Pudiendo así finalmente obtener una visualización del sistema, para facilitar la interpretación del mismo.