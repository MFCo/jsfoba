package ui;



import javax.swing.JFrame;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

public class GraphPanel extends JFrame{
    private static final long serialVersionUID = 1L;
    mxGraph graph;
    Object parent;

    public GraphPanel(Graph g){
    	super("Grafo Resultante");
    	this.graph=g.getAsociatedGraph();
        mxGraphComponent graphComponent = new mxGraphComponent(graph);
		getContentPane().add(graphComponent);
		graphComponent.setConnectable(false);
    }

    public void Update(){

    }
}

