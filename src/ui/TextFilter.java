package ui;

import java.io.File;

 class TextFilter extends javax.swing.filechooser.FileFilter {
    public boolean accept(File file) {
            return file.isDirectory() || file.getAbsolutePath().endsWith(".txt");
        }
    public String getDescription() {
            return "Text documents (*.txt)";
        }
    } 