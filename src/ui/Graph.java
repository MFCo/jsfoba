package ui;

import java.util.ArrayList;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import structure.Call;
import structure.Method;
import structure.Module;
import structure.Program;

public class Graph {
	mxGraph graph;
	Program p;
    Object parent;

    public Graph(Program p){
    	this.p = p;
        this.graph = new mxGraph() 
        {@Override
    	      public boolean isCellSelectable(Object cell) {
	            if (cell != null) {
	               if (cell instanceof mxCell) {
	                  mxCell myCell = (mxCell) cell;
	                  if (myCell.isEdge())
	                     return false;
	               }
	            }
	            return super.isCellSelectable(cell);
	         }
	   };
        this.parent = graph.getDefaultParent();
        graph.getModel().beginUpdate();
        ArrayList <Module> result= p.getModules();
        ArrayList<Object> vertex = new ArrayList<Object>();
        Object global= graph.insertVertex(parent, null, "glob", 20, 20, 180, 30);
        try{
        	for (int i=0; i<result.size();i++){
    			Module mod = result.get(i);
    			ArrayList <Method> resultM=(ArrayList<Method>) mod.getElements(Module.METHOD);
    			for (int j=0; j<resultM.size();j++){
    				Object v1;
    				if(resultM.get(j).getCalls().size()>0)
    					v1 = graph.insertVertex(parent, null,result.get(i).getName()+"."+resultM.get(j).getName(), 20, 20, 180, 30);
    				else
    					v1 = graph.insertVertex(parent, null,result.get(i).getName()+"."+resultM.get(j).getName(), 20, 20, 180, 30,"fillColor=red");
    	            ArrayList<Object> foo = new ArrayList<Object>();
    	            foo.add(v1);
    	            foo.add(resultM.get(j));
    	            vertex.add(foo);
    			}
    			
    		}
        	for (int l = 0; l<vertex.size(); l++){
        		Method meth = (Method) ((ArrayList<?>) vertex.get(l)).get(1);
        		ArrayList<Call> callsMeth = meth.getCalls();
        		for (int h=0; h<callsMeth.size(); h++){
        			Call c = callsMeth.get(h);
        			int o = 0;
        			boolean find=false;
        			while (o<vertex.size() && !find){
        				Method newMeth = (Method) ((ArrayList<?>) vertex.get(o)).get(1); 
        				if(c.getName().equals(newMeth.getFather().getName()+"."+newMeth.getName())){
        					find=true;
        					graph.insertEdge(parent, null, c.getCant(),((ArrayList<?>)vertex.get(o)).get(0) , ((ArrayList<?>)vertex.get(l)).get(0));
        				}
        				else if(c.getName().equals("glob"))
        					graph.insertEdge(parent, null, c.getCant(),global , ((ArrayList<?>)vertex.get(l)).get(0));
        				o++;
        			}
        		}
        	}
        }
        finally{
            graph.getModel().endUpdate();
        }
        graph.setResetEdgesOnMove(true);
    }
    
    public mxGraph getAsociatedGraph(){return graph;}
}
