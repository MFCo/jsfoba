package structure;

public class Call {
    private Method target;
    private String name;
    private int cant;
    public Call(Method target, String name){
        this.target=target;
        this.name=name;
        cant = 1;
    }
    public void addCall(){cant++;}
    public Method getTarget(){return target;}
    public String getName(){return name;}
    public int getCant(){return cant;}
}
