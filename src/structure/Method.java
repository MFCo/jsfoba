package structure;
import java.util.ArrayList;

/**
 *
 * @author Mariano
 */
public class Method extends ModuleElement {

    private ArrayList<Call> invokedBy;
    public Method(){invokedBy = new ArrayList<Call>();}
        
    public void setInvoke(Call c){
    	boolean find = false;
    	for(int i=0;i<invokedBy.size();i++)
    		if(invokedBy.get(i).getName().equals(c.getName())){
    			find=true;
    			invokedBy.get(i).addCall();
    		}    			
    	if (!find)
    		invokedBy.add(c);
    }
    

    
    public ArrayList<Call> getCalls (){return invokedBy;}    


    
    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals (Object obj) {
    if (obj instanceof Method) {
        Method tmp = (Method) obj;
        return super.equals(tmp) && this.getName().equals(tmp.getName());
        }  else { return false; }
    }
}

