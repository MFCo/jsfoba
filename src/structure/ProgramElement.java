package structure;

import java.util.ArrayList;

public abstract class ProgramElement {
	
    private String name;
	
	public abstract ArrayList<?> getElements(String s);
	public abstract void setElement(ModuleElement m, String s);
    public String getName (){return name;}   
    public void setName(String name){this.name=name;}

}
