package structure;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mariano
 */
public class Program {
    private ArrayList<Module> modules;
    private Method global;
    public Program(){
    	modules = new ArrayList<Module>();
    	global=new Method();
    	global.setFather(null);
    	global.setName("glob");
    	}
                
    public void setModule(ProgramElement m){
        modules.add((Module) m);
    }
    
    private Method findMethod(String s){
    	if(s.equals("glob")) return global;
    	int separator = s.indexOf('.');
    	String module = s.substring(0, separator);
    	String method = s.substring(separator+1);
    	for(int i= 0; i<modules.size() ; i++){
    		Module m = modules.get(i);
    		if(m.getName().equals(module)){
	    		ArrayList<Method> arrayList = (ArrayList<Method>) m.getElements(Module.METHOD);
				ArrayList<Method> meth = arrayList;
	    		for (int j =0; j<meth.size();j++){
	    			if(meth.get(j).getName().equals(method))
	    				return meth.get(j);
	    		}
    		}
    	}
		return null;
    }
    
    public void linkProgram(List<?> calls){
    	for(int i=0;i<calls.size();i++){
    		ArrayList<?> tmp = (ArrayList<?>) calls.get(i);
    		Method m = null;
   			m = findMethod((String)tmp.get(0));
    		Call c = new Call(m,(String)tmp.get(0));
    		((Method) tmp.get(1)).setInvoke(c);
    	}
    }
    
    public ArrayList<Module> getModules(){return modules;}   
}
