package structure;

import java.util.ArrayList;

/**
 *
 * @author Mariano
 */
public class Module extends ProgramElement {
	public static final String METHOD = "meth";

    private ArrayList <Method> methods;    
    public Module(){methods = new ArrayList<Method>();}
              
    public void setElement(ModuleElement  m, String s){
    	if(s.equals(METHOD))
    		methods.add((Method) m);
    }
    
    public ArrayList<?> getElements(String s){
    	if(s.equals(METHOD))
    		return methods;
    	return null;
    }
    
    @Override
    public boolean equals (Object obj) {
        if (obj instanceof Module) {
            Module tmp = (Module) obj;
            return super.equals(tmp) && this.getName().equals(tmp.getName());
        }  else { return false; }
    }

}
