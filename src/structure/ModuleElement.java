package structure;

public abstract class ModuleElement {
    private String name;
    private Module father;
    
    public void setName(String name){ this.name=name;}

    
    public String getName (){return name;}  
    
    public void setFather(Module m){
    	father=m;
    }
    
    public Module getFather (){return father;}

}
