var __hidden__ = {};
var stack=[]; //RESULTADO FINAL
var global=['glob']; //PILA DE EJECUCION AUXILIAR
(function () {
  var program;

//AST PARA EL ARBOL
  __hidden__.Ast = function (ast, url) {
    program = ast;
  };


//APPLY PARA SACAR CADA UNO DE LOS LLAMADOS
  __hidden__.apply = function (fct, ths, args, idx) {
    if(ths!==undefined && fct!==undefined){
    var last = global[global.length-1];
    var c = {
      'fcts': fct,
      'obj': ths
    };
    global.push(c);
    var caller;
    if (last!='glob')
      caller = last.obj.constructor.name+'.'+getID(last.fcts,last.obj);
    else
      caller = 'glob';
     for (var k = 0 ; k<stack.length ; k++){
      if (stack[k].id===ths.constructor.name){
        var aux = stack[k].__fcts__;
        for(var i = 0 ; i<aux.length ; i++){
          var foo = stack[k].__fcts__[i].value;
          for(var t = 0 ; t<foo.length ; t++)
            if(foo[t]==fct){
              aux[i].calls.push(caller);
            }
        }
      }
    }
    var result = fct.apply(ths,args);
    global.pop();
    return result;
  }
  };


//DECLARE PARA LEVANTAR CADA UNO DE LOS OBJETOS INVOLUCRADOS
  __hidden__.Declare = function (vs, i){
   var node;
    if (node = search(program, arguments[arguments.length-1])){
      if(node.type==='FunctionDeclaration'){
        var a = {
        'id' : i[0],
        '__fcts__' : []
      };
        console.log(a.id);
        stack.push(a);
      }
    }  
  };


//SET PARA ARMAR LAS FUNCIONES ASOCIADAS A CADA OBJETO
  __hidden__.set = function (object, key, value, index){
    for (var k = 0 ; k<stack.length ; k++){
      if (stack[k].id===object.constructor.name){
        var exist = false;
        for (var i =0 ; i<stack[k].__fcts__.length ; i++)
          if(stack[k].__fcts__[i].key==key){
            stack[k].__fcts__[i].value.push(value);
            exist=true;
          }
        var b = {
          'key' : key,
          'value' : [],
          'calls' : []
        };
        b.value.push(value);
        if(!exist)
          stack[k].__fcts__.push(b); 
      }
    }
    return object[key]=value;   
  };


//FUNCION DE BUSQUEDA DENTRO DEL ARBOL SINTACTICO
  function search (ast, idx) {
    var tmp;
    if (typeof ast !== "object" || ast === null)
      return;
    if (ast.bounds && idx === ast.bounds[0])
      return ast;
    if (ast.bounds && (idx < ast.bounds[0] || idx > ast.bounds[1]))
      return;
    for (var k in ast)
      if (tmp = search(ast[k], idx))
        return tmp;
  }


//FUNCION DE BUSQUEDA EN LA PILA
  function getID (fct, obj){
     for (var k = 0 ; k<stack.length ; k++){
      if (stack[k].id===obj.constructor.name){
        var aux = stack[k].__fcts__;
        for(var i = 0 ; i<aux.length ; i++){
          var foo = stack[k].__fcts__[i].value;
          for(var t = 0 ; t<foo.length ; t++)
            if(foo[t]==fct){
              return aux[i].key;
            }
        }
      }
    }
    return null; 
  }
} ());