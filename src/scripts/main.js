var fs = require('fs');
var Aran = require('aran');
var analysis = fs.readFileSync(__dirname+'/analysis.js', {encoding:'utf8'});
var target = fs.readFileSync(__dirname+'/target.js', {encoding:'utf8'});
var aran = Aran({namespace:'__hidden__', traps:['Ast','apply','Declare','set'], loc:true});
var instrumented = aran(target);
var cout =  fs.readFileSync(__dirname+'/print.js', {encoding:'utf8'});
fs.writeFileSync(__dirname+'/__target__.js', analysis+'\n'+instrumented+'\n'+cout);
