function Account(type){
  var type = type;
  
  this.getType=function(){
  	return type;
  }
    
}

function Cliente(){
	  var egresos = 1980;
	  var ingresos = 2000;
	  
	  this.getEgresos=function(){
	  	return egresos;
	  }
	  
	  this.getIngresos=function(){
	    return ingresos;
	  }
		     
}

function Calculator(cliente){
  
	  this.calculateBalance=function(){
	  	return cliente.getIngresos()-cliente.getEgresos();
	  }
	    
}

Account.prototype.getBalance = function(cliente){
    var calculator = new Calculator(cliente);
    return calculator.calculateBalance(cliente);
}

var account = new Account("CA");
var cliente = new Cliente();
account.getBalance(cliente);